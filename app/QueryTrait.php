<?php

namespace App;

trait QueryTrait
{
    private function valuesToString()
    {
        $having = $this->fixHavingStatement($this->having);
        $whereConditions = $this->fixHavingStatement($this->whereConditions);
        $select = !empty($this->select) ? implode(',', $this->select) : '*';
        $from = !empty($this->from) ? implode(',', $this->from) : $this->defaultTable;
        $groupBy = !empty($this->groupBy) ? implode(',', $this->groupBy) : '';
        $orderBy = !empty($this->orderBy) ? implode(',', $this->orderBy) : '';


        return [
            'select'          => $select,
            'from'            => $from,
            'whereConditions' => $whereConditions,
            'groupBy'         => $groupBy,
            'having'          => $having,
            'orderBy'         => $orderBy
        ];
    }

    private function generateSqlString(): string
    {
        $fields = $this->valuesToString();
        $sql = "SELECT ${fields['select']} FROM ${fields['from']} ";

        if (!empty($fields['whereConditions'])) {
            $sql .= "WHERE ${fields['whereConditions']} ";
        }

        if (!empty($fields['groupBy'])) {
            $sql .= "GROUP BY ${fields['groupBy']} ";
        }

        if (!empty($fields['having'])) {
            $sql .= "HAVING ${fields['having']} ";
        }

        if (!empty($fields['orderBy'])) {
            $sql .= "ORDER BY ${fields['orderBy']} ";
        }

        if (!empty($this->limit) && !empty($this->offset)) {
            $sql .= "LIMIT $this->offset, $this->limit";
        } else if (!empty($this->limit)) {
            $sql .= "LIMIT $this->limit";
        }

        return $sql;
    }

    private function saveTables($table)
    {
        if (is_object($table)) {
            $this->from = array_merge($this->from, $table->from);
        } else {
            $this->from[] = $table;
        }
    }

    private function fixHavingStatement(array $having): string
    {
        $string = '';

        if (!empty($having)) {
            foreach ($having as $k => $item) {

                if (strpos($item, '=')) {
                    $exploded = explode('=', $item);

                    if (strpos($exploded[1], "'") === false) {
                        $string .= "$exploded[0]='$exploded[1]'";

                        if (isset($having[$k + 1])) {
                            $string .= ' AND ';
                        }
                    }
                }
            }

        }

        return $string;
    }

}