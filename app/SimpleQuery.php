<?php

namespace App;

use LogicException;
use PDO;
use PDOException;

class SimpleQuery implements SimpleQueryBuilderInterface
{
    use QueryTrait;

    public $defaultTable = 'users';

    public $select = [], $from = [], $limit, $offset, $whereConditions = [], $groupBy = [], $having = [], $orderBy = [];
    private $dbConnection;

    public function __construct($host = 'localhost', $dbName = 'test', $user = 'root', $password = '')
    {
        $this->dbConnection = new PDO("mysql:host=$host;dbname=$dbName", $user, $password);
        $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @param array|string $fields
     * @return SimpleQueryBuilderInterface
     */
    public function select($fields = ['*']): SimpleQueryBuilderInterface
    {
        if (is_array($fields)) {
            $this->select = array_merge($this->select, $fields);
        } else {
            $this->select[] = $fields;
        }

        return $this;
    }

    /**
     * @param string|SimpleQueryBuilderInterface|array<string|SimpleQueryBuilderInterface> $tables
     * @return SimpleQueryBuilderInterface
     */
    public function from($tables): SimpleQueryBuilderInterface
    {
        if (is_array($tables)) {
            foreach ($tables as $table) {
                $this->saveTables($table);
            }
        } else {
            $this->saveTables($tables);
        }

        return $this;
    }

    /**
     * @param string|array $conditions
     * @return SimpleQueryBuilderInterface
     */
    public function where($conditions): SimpleQueryBuilderInterface
    {
        if (is_array($conditions)) {
            $this->whereConditions = array_merge($this->whereConditions, $conditions);
        } else {
            $this->whereConditions[] = $conditions;
        }

        return $this;
    }

    /**
     * @param string|array $fields
     * @return SimpleQueryBuilderInterface
     */
    public function groupBy($fields): SimpleQueryBuilderInterface
    {
        if (is_array($fields)) {
            $this->groupBy = array_merge($this->groupBy, $fields);
        } else {
            $this->groupBy[] = $fields;
        }

        return $this;
    }

    /**
     * @param string|array $conditions
     * @return SimpleQueryBuilderInterface
     */
    public function having($conditions): SimpleQueryBuilderInterface
    {
        if (is_array($conditions)) {
            $this->having = array_merge($this->having, $conditions);
        } else {
            $this->having[] = $conditions;
        }

        return $this;
    }

    /**
     * @param string|array $fields
     * @return SimpleQueryBuilderInterface
     */
    public function orderBy($fields): SimpleQueryBuilderInterface
    {
        if (is_array($fields)) {
            $this->orderBy = array_merge($this->orderBy, $fields);
        } else {
            $this->orderBy[] = $fields;
        }

        return $this;
    }

    /**
     * @param int $limit
     * @return SimpleQueryBuilderInterface
     */
    public function limit($limit): SimpleQueryBuilderInterface
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @param int $offset
     * @return SimpleQueryBuilderInterface
     */
    public function offset($offset): SimpleQueryBuilderInterface
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @return string
     * @throws LogicException
     */
    public function build(): string
    {
        try {
            $sql = $this->generateSqlString();
            $request = $this->dbConnection->prepare($sql);
            $request->execute();
            return json_encode($request->fetchAll(PDO::FETCH_ASSOC));

        } catch (PDOException $exception) {

            return json_encode('Incorrect data given');
        }
    }

    /**
     * @return string
     * @throws LogicException
     */
    public function buildCount(): string
    {
        $records = json_decode($this->build(), true);

        return json_encode(count($records));
    }
}