<?php

use PHPUnit\Framework\TestCase;
use \App\SimpleQuery;

class Tests extends TestCase
{
    public function testWhere()
    {
        $data = json_decode((new SimpleQuery())->from('tasks')->where('id = 1')->build(), true);
        $check = $data[0]['id'] == 1;
        $this->assertTrue($check);
        $data = json_decode((new SimpleQuery())->from('tasks')->where(['id = 1', 'email=ilya@gmail.com'])->build(), true);
        $checkId = $data[0]['id'] == 1;
        $checkEmail = $data[0]['email'] == 'ilya@gmail.com';
        $this->assertTrue($checkId);
        $this->assertTrue($checkEmail);
        return $this;
    }

    public function testSelect()
    {
        $data = json_decode((new SimpleQuery())->select('id')->from('tasks')->build(), true);
        $check = isset($data[0]['email']);
        $checkId = isset($data[0]['id']);
        $this->assertFalse($check);
        $this->assertTrue($checkId);

        $data = json_decode((new SimpleQuery())->select(['id', 'email'])->from('tasks')->build(), true);
        $checkId = isset($data[0]['id']);
        $checkEmail = isset($data[0]['email']);
        $checkTask = isset($data[0]['task']);
        $this->assertTrue($checkId);
        $this->assertTrue($checkEmail);
        $this->assertfalse($checkTask);
    }

    public function testFrom()
    {
        $data = json_decode((new SimpleQuery())->from('tasks')->build(), true);
        $check = isset($data[0]['task']);
        $this->assertTrue($check);

        $data = json_decode((new SimpleQuery())->from(['tasks', 'users'])->build(), true);
        $checkTask = isset($data[0]['task']);
        $checkLogin = isset($data[0]['login']);
        $this->assertTrue($checkTask);
        $this->assertTrue($checkLogin);
    }

    public function testLimit()
    {
        $data = json_decode((new SimpleQuery())->from('tasks')->limit(3)->build(), true);
        $check = count($data) <= 3;
        $this->assertTrue($check);
    }

    public function testOffset()
    {
        $data = json_decode((new SimpleQuery())->from('tasks')->build(), true);

        $dataWithOffset = json_decode((new SimpleQuery())->from('tasks')->limit(3)->offset(0)->build(), true);
        $check = $dataWithOffset[0]['id'] == $data[0]['id'];
        $this->assertTrue($check);

        $dataWithOffset = json_decode((new SimpleQuery())->from('tasks')->limit(3)->offset(3)->build(), true);
        $check = $dataWithOffset[0]['id'] == $data[3]['id'];
        $this->assertTrue($check);
    }

    public function testGroupBy()
    {
        $data = json_decode((new SimpleQuery())->from('tasks')->groupBy('first_name')->build(), true);
        $check = array_search(6, array_column($data, 'id'));
        $this->assertFalse($check);
    }

    public function testHaving()
    {
        $data = json_decode((new SimpleQuery())->from('tasks')->having('id = 1')->build(), true);
        $check = $data[0]['id'] == 1;
        $this->assertTrue($check);
        $data = json_decode((new SimpleQuery())->from('tasks')->having(['id = 1', 'email=ilya@gmail.com'])->build(), true);
        $checkId = $data[0]['id'] == 1;
        $checkEmail = $data[0]['email'] == 'ilya@gmail.com';
        $this->assertTrue($checkId);
        $this->assertTrue($checkEmail);
        return $this;
    }

    public function testOrderBy()
    {
        $data = array_reverse(json_decode((new SimpleQuery())->from('tasks')->build(), true));

        $dataWithOrder = json_decode((new SimpleQuery())->from('tasks')->orderBy('id desc')->build(), true);
        $check = $data[0]['id'] == $dataWithOrder[0]['id'];
        $this->assertTrue($check);

        return $this;
    }
}