-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 28 2020 г., 14:17
-- Версия сервера: 5.6.43
-- Версия PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `task` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `edited` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tasks`
--

INSERT INTO `tasks` (`id`, `first_name`, `email`, `task`, `status`, `edited`) VALUES
(1, 'Илья', 'ilya@gmail.com', 'qweqweqw eqe', 1, 1),
(2, 'Viktor', NULL, 'qweqweqwe wq', 0, 1),
(3, 'Sergey', 'sergey@gmail.com', 'qwe qweq e', 0, 0),
(4, 'George', 'georgy@gmail.com', 'qwe qw eqw eqe', 0, 0),
(5, 'Oleg', 'oleg@gmail.com', 'qw ew eqw eqe', 0, 0),
(6, 'George', 'george@gmail.com', 'qwe qweq e', 0, 0),
(7, 'Allan', 'sample1@gmail.com', 'task 1', 0, 0),
(8, 'Allena', 'asda@qw.qw', 'qwe qwe', 0, 0),
(9, 'Elena', 'test@qwe.qw', 'test', 0, 0),
(10, 'Chester', 'test.chester@gmail.com', 'test', 0, 0),
(11, 'Anna', 'anna@gmail.com', 'test', 0, 0),
(12, 'Mila', 'vkvk@gmail.com', 'asdasd', 0, 0),
(13, 'Katy', 'qeqwe@asdq.qw', 'qweqw e', 0, 0),
(14, 'Anton', 'asdsa@qwe.qw', 'asd asdq', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `first_name`, `login`, `password`) VALUES
(1, 'Admin', 'admin', '202cb962ac59075b964b07152d234b70');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
