<?php
require_once('vendor/autoload.php');

use App\SimpleQuery;

$query = (new SimpleQuery());

print_r(json_decode($query->from(['tasks'])->build(), true));

